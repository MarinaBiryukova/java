public class Task_2 {
    public static void main(String[] args) {
        int[] arr = new int[] {3, 6, 8, 2, 5, 1, 4, 0, 9, 7};
        System.out.println("Исходный массив:");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.print("\n");
        int[] index = new int[arr.length];
        for (int i = 0; i < index.length; i++) {
            index[i] = (int) (Math.random() * index.length);
            boolean isUnique = false;
            while (!isUnique) {
                if (index[i] == i) {
                    index[i] = (int) (Math.random() * index.length);
                    continue;
                }
                int j;
                for (j = 0; j < i; j++) {
                    if (index[i] == index[j]) {
                        index[i] = (int) (Math.random() * index.length);
                        break;
                    }
                }
                if (j == i) {
                    isUnique = true;
                }
            }
        }
        int[] temp = new int[arr.length];
        System.arraycopy(arr, 0, temp, 0, arr.length);
        for (int i = 0; i < arr.length; i++) {
            arr[i] = temp[index[i]];
        }
        System.out.println("Полученный массив:");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
