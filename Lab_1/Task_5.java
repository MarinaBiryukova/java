public class Task_5 {
    public static void main(String[] args) {
        int rows = (int) (2 + (Math.random() * 10));
        int columns = (int) (2 + (Math.random() * 10));
        int [][] matrix = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                matrix[i][j] = (int) (Math.random() * 100);
            }
        }
        System.out.println("Исходная матрица:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.print("\n");
        }
        int[] max = new int[rows];
        for (int i = 0; i < rows; i++) {
            max[i] = matrix[i][0];
            for (int j = 1; j < columns; j++) {
                if (matrix[i][j] > max[i]) {
                    max[i] = matrix[i][j];
                }
            }
        }
        int minIndex = 0;
        for (int i = 1; i < rows; i++) {
            if (max[i] < max[minIndex]) {
                minIndex = i;
            }
        }
        for (int i = minIndex; i < rows - 1; i++) {
            matrix[i] = matrix[i + 1];
        }
        matrix[rows - 1] = null;
        System.out.println("Полученная матрица:");
        for (int i = 0; i < rows - 1; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.print("\n");
        }
    }
}
