public class Task_4 {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Неверное число аргументов");
            return;
        }
        int rows = Integer.parseInt(args[0]);
        int columns = Integer.parseInt(args[1]);
        System.out.println("Введенные данные: количество строк = " + rows + " количество столбцов = " + columns);
        int [][] matrix = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (j <= i) {
                    matrix[i][j] = 1;
                }
                else {
                    matrix[i][j] = 0;
                }
            }
        }
        System.out.println("Полученная матрица:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print((matrix[i][j]) + " ");
            }
            System.out.print("\n");
        }
    }
}
