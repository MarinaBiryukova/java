public class Task_3 {
    public static void main(String[] args) {
        int[] arr = new int[args.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(args[i]);
        }
        System.out.println("Введенный массив:");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.print("\n");
        int count = 0;
        boolean isUnique = true;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    isUnique = false;
                }
            }
            if (isUnique) {
                count++;
            }
            isUnique = true;
        }
        System.out.println("Количество уникальных элементов: " + count);
    }
}
