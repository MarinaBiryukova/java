public class Task_1 {
    public static void main(String[] args) {
        int[] arr = new int[args.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(args[i]);
        }
        System.out.println("Введенный массив:");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.print("\n");
        int step = arr.length / 2;
        while (step >= 1) {
            for (int j = step; j < arr.length; j++) {
                int k = j - step;
                while (k >= 0 && arr[k] > arr[k + step]) {
                    int temp = arr[k];
                    arr[k] = arr[k + step];
                    arr[k + step] = temp;
                    k -= step;
                }
            }
            step /= 2;
        }
        System.out.println("Отсортированный массив:");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
