package lab_4;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Task_3 {
    public static void main(String[] args) {
        Map<String, Integer> map = new LinkedHashMap<>();
        map.put("First", 1);
        map.put("Second", 2);
        map.put("Third", 3);
        map.put("Fourth", 4);
        map.put("Fifth", 5);
        System.out.println("Исходный map:");
        System.out.println(map);
        System.out.println("Полученный map:");
        System.out.println(method(map));
    }

    public static <K, V> Map<V, K> method(Map<K, V> map) {
        Map<V, K> newMap = new HashMap<>();
        for (K key : map.keySet()) {
            newMap.put(map.get(key), key);
        }
        return newMap;
    }
}
