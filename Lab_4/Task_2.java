package lab_4;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class Task_2 {
    public static void main(String[] args) {
        System.out.print("Введите числа: ");
        Scanner sc = new Scanner(System.in);
        Deque<Integer> stack = new ArrayDeque<>();
        while (sc.hasNext()) {
            if (!sc.hasNextInt()) {
                System.out.print("Введено не число");
                return;
            }
            int number = sc.nextInt();
            int radix = getRadix(number);
            for (int i = radix; i > 0; i--) {
                int temp = (number % (int)Math.pow(10, i)) / (int)Math.pow(10, i - 1);
                stack.add(temp);
            }
            for (int i = stack.size(); i > 0; i--) {
                System.out.print(stack.pollLast());
            }
            System.out.println();
        }

    }

    public static int getRadix(int number) {
        int radix = 0;
        while (number > 0) {
            radix++;
            number /= 10;
        }
        return radix;
    }
}
