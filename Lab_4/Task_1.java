package lab_4;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Task_1 {
    public static void main(String[] args) {
        System.out.print("Введите количество элементов списка: ");
        Scanner sc = new Scanner(System.in);
        if (!sc.hasNextInt()) {
            System.out.print("Введено не число");
            return;
        }
        int size = sc.nextInt();
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            list.add(i);
        }
        System.out.print("Введите число для сдвига: ");
        if (!sc.hasNextInt()) {
            System.out.print("Введено не число");
            return;
        }
        int number = sc.nextInt();
        shift(list, number);
        System.out.println(list);
    }

    public static void shift(List<Integer> list, int n) {
        n %= list.size();
        if (n > 0) {
            for (int i = 0; i < n; i++) {
                int temp = (Integer)list.remove(list.size() - 1);
                list.add(0, temp);
            }
        }
        else {
            for (int i = n; i < 0; i++) {
                int temp = (Integer)list.remove(0);
                list.add(list.size(), temp);
            }
        }
    }
}
