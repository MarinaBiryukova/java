package lab_5;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        int number = 9;
        Class<AnnotationClass.NumberActions> c = AnnotationClass.NumberActions.class;
        AnnotationClass.NumberActions instance = new AnnotationClass.NumberActions();
        try {
            for (Method m:c.getDeclaredMethods()) {
                if (m.isAnnotationPresent(NumberOfTimes.class)) {
                    m.setAccessible(true);
                    int value = m.getAnnotation(NumberOfTimes.class).value();
                    for (int i = 0; i < value; i++) {
                        m.invoke(instance, number);
                    }
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
