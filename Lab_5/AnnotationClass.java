package lab_5;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface NumberOfTimes {
    int value() default 1;
}
public class AnnotationClass {
    public static class NumberActions {
        @NumberOfTimes
        private void printFactorial(int number) throws Exception {
            if (number < 0) {
                throw new Exception("Невозможно посчитать факториал отрицательного числа");
            }
            int result = 1;
            for (int i = 2; i <= number; i++) {
                result *= i;
            }
            System.out.println("Факториал числа " + number + ": " + result);
        }

        @NumberOfTimes(3)
        private void printNumber(int number) {
            System.out.println("Переданное число: " + number);
        }

        private void printSquareArea(int a) throws Exception {
            if (a < 0) {
                throw new Exception("Сторона квадрата должна быть положительным числом");
            }
            System.out.println("Площадь квадрата: " + (a * a));
        }

        @NumberOfTimes(2)
        private void printRootOfNumber(int number) throws Exception {
            if (number < 0) {
                throw new Exception("Невозможо посчитать корень отрицательного числа");
            }
            System.out.println("Корень числа " + number + ": " + Math.sqrt(number));
        }
    }
}
