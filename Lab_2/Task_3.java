package lab_2;

public class Task_3 {
    public static class Point {
        private double x;
        private double y;
        Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
        public double getX() {
            return x;
        }
        public double getY() {
            return y;
        }
        public void setX(double x) {
            this.x = x;
        }
        public void setY(double y) {
            this.y = y;
        }
        public double distanceTo(Point point) {
            return Math.sqrt(Math.pow(point.getX() - this.x, 2) + Math.pow(point.getY() - this.y, 2));
        }
        public void print() {
            System.out.print(" (" + String.format("%.2f", x) + ";" + String.format("%.2f", y) + ") ");
        }
    }

    public static class Triangle {
        private Point a, b, c;
        Triangle(Point a, Point b, Point c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public Point getA() {
            return a;
        }
        public Point getB() {
            return b;
        }
        public Point getC() {
            return c;
        }
        public void setA(Point a) { this.a = a; }
        public void setB(Point b) { this.b = b; }
        public void setC(Point c) { this.c = c; }
        public double getAB() {
            return a.distanceTo(b);
        }
        public double getAC() {
            return a.distanceTo(c);
        }
        public double getBC() {
            return b.distanceTo(c);
        }
        public Point getCenter() {
            double x = (a.x + b.x + c.x) / 3;
            double y = (a.y + b.y + c.y) / 3;
            Point centre = new Point(x, y);
            return centre;
        }
        public double getArea() {
            double p = getPerimeter() / 2;
            return (Math.sqrt(p * (p - getAB()) * (p - getAC()) * (p - getBC())));
        }
        public double getPerimeter() {
            return (getAB() + getAC() + getBC());
        }
        public void printInfo() {
            System.out.print("Вершины треугольника:");
            getA().print();
            getB().print();
            getC().print();
            System.out.println("");
            System.out.println("Стороны треугольника: AB=" + String.format("%.2f", getAB()) + ", AC="
                    + String.format("%.2f", getAC()) + ", BC=" + String.format("%.2f", getBC()));
            System.out.print("Центр треугольника (точка пересечения медиан):");
            getCenter().print();
            System.out.println("");
            System.out.println("Площадь треугольника: S=" + String.format("%.2f", getArea()));
            System.out.println("Периметр треугольника: P=" + String.format("%.2f", getPerimeter()));
        }
    }

    public static void main(String[] args) {
        Point A = new Point(1.0, 4.2);
        Point B = new Point(5.0, 2.0);
        Point C = new Point(0.5, 2.0);
        Triangle triangle = new Triangle(A, B, C);
        triangle.printInfo();
    }
}
