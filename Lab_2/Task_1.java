package lab_2;

import java.util.Scanner;

public class Task_1 {
    static String[] figures = {"ноль", "один ", "два ", "три ", "четыре ", "пять ", "шесть ", "семь ", "восемь ", "девять "};
    static String[] dozens_1 = {"десять ", "одиннадцать ", "двенадцать " , "тринадцать ", "четырнадцать ",
            "пятнадцать ", "шестнадцать ", "семнадцать ", "восемнадцать ", "девятнадцать "};
    static String[] dozens_2 = {"двадцать ", "тридцать ", "сорок ", "пятьдесят ", "шестьдесят ", "семдесят ",
            "восемьдесят ", "девяносто "};
    static String[] hundreds = {"сто ", "двести ", "триста ", "четыреста ", "пятьсот ", "шестьсот ",
            "семьсот ", "восемьсот ", "девятьсот "};

    public static void printUnits(int number) {
        if (Math.abs(number) < 10) {
            System.out.print(figures[Math.abs(number)]);
        }
        else if (Math.abs(number) >= 10 && Math.abs(number) < 20) {
            System.out.print(dozens_1[Math.abs(number) % 10]);
        }
        else if (Math.abs(number) >= 20 && Math.abs(number) < 100){
            System.out.print(dozens_2[(Math.abs(number) / 10) - 2]);
            if (Math.abs(number) % 10 != 0) {
                System.out.print(figures[Math.abs(number) % 10]);
            }
        }
        else {
            System.out.print(hundreds[Math.abs(number) / 100 - 1]);
            if ((Math.abs(number) % 100) >= 10 && (Math.abs(number) % 100) < 20) {
                System.out.print(dozens_1[Math.abs(number) % 10]);
            }
            else {
                System.out.print(dozens_2[((Math.abs(number) % 100) / 10) - 2]);
                if (Math.abs(number) % 10 != 0) {
                    System.out.print(figures[Math.abs(number) % 10]);
                }
            }
        }
    }

    public static void printThousands(int number) {
        String thousand;
        if (number == 1) {
            System.out.print("одна тысяча ");
            return;
        }
        if (number == 2) {
            System.out.print("две тысячи ");
            return;
        }
        if (number % 100 >= 11 && number % 100 <= 19) {
            thousand = "тысяч ";
        }
        else if (number % 10 == 1) {
            thousand = "тысяча ";
        }
        else if (number % 10 >= 2 && number % 10 <= 4) {
            thousand = "тысячи ";
        }
        else {
            thousand = "тысяч ";
        }
        printUnits(number);
        System.out.print(thousand);
    }

    public static void printMillions(int number) {
        String million;
        if (number % 100 >= 11 && number % 100 <= 19) {
            million = "миллионов ";
        }
        else if (number % 10 == 1) {
            million = "миллион ";
        }
        else if (number % 10 >= 2 && number % 10 <= 4) {
            million = "миллиона ";
        }
        else {
            million = "миллионов ";
        }
        printUnits(number);
        System.out.print(million);
    }

    public static void printBillions(int number) {
        if (number == 1) {
            System.out.print("один миллиард ");
        }
        else if (number == 2) {
            System.out.print("два миллиарда ");
        }
    }

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        Scanner sc = new Scanner(System.in);
        if (!sc.hasNextInt()) {
            System.out.print("Введено не число");
            return;
        }
        int number = sc.nextInt();
        if (number < 0) {
            System.out.print("минус ");
        }
        int copy_number = number;
        int radix = 0;
        while (Math.abs(copy_number) > 0) {
            radix++;
            copy_number /= 10;
        }
        if (radix >= 0 && radix <= 3) {
            printUnits(number);
        }
        else if (radix >= 4 && radix <= 6) {
            printThousands(number / 1000);
            if (number % 1000 != 0) {
                printUnits(number % 1000);
            }
        }
        else if (radix >= 7 && radix <= 9) {
            printMillions(number / 1000000);
            if (number % 1000000 / 1000 != 0) {
                printThousands(number % 1000000 / 1000);
            }
            if (number % 1000 != 0) {
                printUnits(number % 1000);
            }
        }
        else {
            printBillions(number / 1000000000);
            if (number % 1000000000 / 1000000 != 0) {
                printMillions(number % 1000000000 / 1000000);
            }
            if (number % 1000000 / 1000 != 0) {
                printThousands(number % 1000000 / 1000);
            }
            if (number % 1000 != 0) {
                printUnits(number % 1000);
            }
        }
    }
}
