package lab_2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

public class Task_2 {
    public static void main(String[] args) {
        BufferedReader reader;
        try {
            File file = new File("logins.txt");
            FileReader fr = new FileReader(file);
            reader = new BufferedReader(fr);
            Map<String, Integer> passwords = new TreeMap<String, Integer>();
            String line = reader.readLine();
            while (line != null) {
                String[] login = line.split(" ");
                if (passwords == null || !passwords.containsKey(login[1])) {
                    passwords.put(login[1], (int) 1);
                }
                else {
                    passwords.replace(login[1], passwords.get(login[1]), (int) passwords.get(login[1]) + 1);
                }
                line = reader.readLine();
            }
            for (int i = 0; i < 3; i++) {
                int max = 0;
                String top = new String();
                for(Entry entry: passwords.entrySet()) {
                    if ((int) entry.getValue() > max) {
                        top = (String) entry.getKey();
                        max = (int) entry.getValue();
                    }
                }
                System.out.println(i + 1 + ": " + top);
                passwords.remove(top);
            }
        }
        catch (FileNotFoundException e) {
            e.getStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
