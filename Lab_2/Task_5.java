package lab_2;

import java.util.Arrays;
import java.util.Scanner;

public class Task_5 {
    public static void main(String[] args) {
        System.out.print("Введите строку: ");
        Scanner sc = new Scanner(System.in);
        StringBuilder input = new StringBuilder(sc.nextLine());
        if (input.length() % 3 != 0) {
            System.err.println("Длина строки не кратна трем");
            return;
        }
        StringBuilder [] frag = new StringBuilder[input.length() / 3];
        for (int j = 0; j < input.length(); j =j + 3) {
            frag[j / 3] = new StringBuilder(String.valueOf(input.charAt(j)) + String.valueOf(input.charAt(j + 1))
                    + String.valueOf(input.charAt(j + 2)));
        }
        for (int j = 0; j < frag.length; j++) {
            char c = (char)((int) ((int) 'a' + Math.random() * 26));
            while (c == frag[j].charAt(0) || c == frag[j].charAt(2)) {
                c = (char)((int) ((int) 'a' + Math.random() * 26));
            }
            frag[j].replace(1, 2, String.valueOf(c));
        }
        Arrays.sort(frag);
        for (int j = 0; j < frag.length; j++) {
            System.out.println(frag[j]);
        }
        sc.close();
    }
}
