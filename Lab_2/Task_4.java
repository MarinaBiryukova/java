package lab_2;

import java.util.Scanner;

public class Task_4 {
    public static void main(String[] args) {
        System.out.print("Введите строку: ");
        Scanner sc = new Scanner(System.in);
        StringBuilder input = new StringBuilder(sc.nextLine());
        int i = 0, count = 0, begin = 0, end = 0;
        while (input.charAt(i) == ' ') {
            count++;
            i++;
        }
        end = i;
        if (count > 0) {
            input.delete(begin, end);
        }
        count = 0;
        for (int j = 0; j < input.length(); j++) {
            if (input.charAt(j) == ' ') {
                if (count == 0) {
                    count++;
                }
                else if (count == 1) {
                    count++;
                    begin = j;
                    end = j;
                }
                else if (count > 1) {
                    count++;
                    end = j;
                }
            }
            else {
                if (count > 1) {
                    input.delete(begin, end + 1);
                    count = 0;
                    begin = 0;
                    end = 0;
                }
            }
        }
        if (input.charAt(input.length() - 1) == ' ') {
            input.deleteCharAt(input.length() - 1);
        }
        System.out.print("Исправленная строка: ");
        System.out.println(input);
        sc.close();
    }
}
